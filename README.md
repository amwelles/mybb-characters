# Installation

- Upload `characters.php` to `/inc/plugins`.
- Add `{$characters}` somewhere in the `member_profile` template.
- Link to `/usercp.php?action=characters` somewhere in the User CP.
- Link to `/misc.php?action=characters` for a list of all characters.
- Add the character select box to the following templates below `{$loginbox}`:
	- `editpost`
	- `newthread`
	- `newreply`

		```
		<tr>
		<td class="trow2"><strong>Character</strong></td>
		<td class="trow2">{$characters_select}</td>
		</tr>
		```
- Add `{$mybb->user['character']}` somwere in the `postbit` and `postbit_classic` template.