<?php

// Disallow direct access to this file for security reasons
if(!defined("IN_MYBB"))
{
	die("Direct initialization of this file is not allowed.");
}

function characters_info() {
	return array(
		'name' => 'Characters',
		'description' => 'Allows characters to be attached to posts.',
		'author' => 'Autumn Welles',
		'authorsite' => 'http://autumnwelles.com/',
		'version' => '0.1',
		'compatibility' => '*'
	);
}

function characters_install() {

	global $db, $mybb;

	// Create characters table
	$db->write_query("CREATE TABLE ". $db->table_prefix ."characters(
		`cid` INT(10) unsigned NOT NULL AUTO_INCREMENT,
		`uid` INT(10) unsigned NOT NULL DEFAULT '0',
		`name` varchar(255) NOT NULL DEFAULT '',
		`link` varchar(255) NOT NULL DEFAULT '',
		PRIMARY KEY (`cid`),
		KEY `uid` (`uid`)
	);");

	// Add characters column to posts
	$db->write_query("ALTER TABLE `". $db->table_prefix ."posts`
		ADD `cid` INT(10) unsigned NOT NULL DEFAULT '0',
		ADD KEY (`cid`)
	;");

	// Define character templates
	$characters_nav = '<tr><td class="trow1 smalltext"><a href="usercp.php?action=characters" class="usercp_nav_item usercp_nav_options">Manage Characters</a></td></tr>';
	$characters_mgmt = '<html>
<head>
<title>{$lang->user_cp}</title>
{$headerinclude}
</head>
<body>
{$header}
<table width="100%" border="0" align="center">
<tr>
{$usercpnav}
<td valign="top">
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="{$colspan}"><strong>Manage Characters</strong></td>
</tr>
<tr>
<td class="trow1">
<form action="usercp.php?action=characters_create" method="post">
<strong>Create a new character</strong><br>
<input type="text" name="character_name" class="textbox" required>
<input type="submit" value="Create" class="button">
</form>
</td>
</tr>
<tr>
<td class="trow1">
<form action="usercp.php?action=characters_update" method="post">
<strong>Update existing characters</strong><br>
{$characters_rows}
<input type="submit" value="Update" class="button">
</form>
</td>
</tr>
</table>
</td>
</tr>
</table>
{$footer}
</body>
</html>';
	$characters_row = '<input type="text" value="{$character[\'name\']}" name="name[{$character[\'cid\']}]" class="textbox" required> <input type="url" value="{$character[\'link\']}" name="link[{$character[\'cid\']}]" class="textbox" placeholder="https://"><br>';
	$characters_profile = '<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder tfixed" width="100%">
<colgroup>
<col style="width: 30%;" />
</colgroup>
<tr>
<td class="thead"><strong>Characters</strong></td>
</tr>
<tr><td class="trow1">
<ul>
{$character_rows}
</ul>
</td></tr>
</table>
<br />';
	$characters_profile_row = '<li>
<a href="{$character[\'link\']}">
{$character[\'name\']}
</a>
</li>';
	$characters_list = '<html>
<head>
<title>{$mybb->settings[\'bbname\']} - Characters</title>
{$headerinclude}
</head>
<body>
{$header}
<table border="0" cellspacing="{$theme[\'borderwidth\']}" cellpadding="{$theme[\'tablespace\']}" class="tborder">
<tr>
<td class="thead" colspan="2">
<strong>Characters</strong>
</td>
</tr>
<tr>
<td class="tcat">Name</td>
<td class="tcat">Played by</td>
</tr>
{$character_rows}
</table>
<br />
{$footer}
</body>
</html>';
	$characters_list_row = '<tr>
<td>{$character[\'name\']}</td>
<td>{$character[\'user\']}</td>
</tr>';

	// Create character templates
	$insert_characters_nav = array(
		'title' => 'characters_nav',
		'template' => $db->escape_string($characters_nav),
		'sid' => '-1',
		'version' => '',
		'dateline' => time()
	);
	$insert_characters_mgmt = array(
		'title' => 'characters_mgmt',
		'template' => $db->escape_string($characters_mgmt),
		'sid' => '-1',
		'version' => '',
		'dateline' => time()
	);
	$insert_characters_row = array(
		'title' => 'characters_row',
		'template' => $db->escape_string($characters_row),
		'sid' => '-1',
		'version' => '',
		'dateline' => time()
	);
	$insert_characters_profile = array(
		'title' => 'characters_profile',
		'template' => $db->escape_string($characters_profile),
		'sid' => '-1',
		'version' => '',
		'dateline' => time()
	);
	$insert_characters_profile_row = array(
		'title' => 'characters_profile_row',
		'template' => $db->escape_string($characters_profile_row),
		'sid' => '-1',
		'version' => '',
		'dateline' => time()
	);
	$insert_characters_list = array(
		'title' => 'characters_list',
		'template' => $db->escape_string($characters_list),
		'sid' => '-1',
		'version' => '',
		'dateline' => time()
	);
	$insert_characters_list_row = array(
		'title' => 'characters_list_row',
		'template' => $db->escape_string($characters_list_row),
		'sid' => '-1',
		'version' => '',
		'dateline' => time()
	);

	// Insert character templates
	$db->insert_query('templates', $insert_characters_nav);
	$db->insert_query('templates', $insert_characters_mgmt);
	$db->insert_query('templates', $insert_characters_row);
	$db->insert_query('templates', $insert_characters_profile);
	$db->insert_query('templates', $insert_characters_profile_row);
	$db->insert_query('templates', $insert_characters_list);
	$db->insert_query('templates', $insert_characters_list_row);

}

function characters_is_installed() {

	global $db, $mybb;

	if($db->table_exists('characters')) {
		return true;
	}
	return false;

}

function characters_uninstall() {

	global $db, $mybb;

	// Delete characters table
	$db->write_query("DROP TABLE ". $db->table_prefix ."characters;");

	// Delete characters column from posts
	$db->write_query("ALTER TABLE `". $db->table_prefix ."posts` DROP `cid`;");

	// Delete character templates
	$db->write_query("DELETE FROM `". $db->table_prefix ."templates` WHERE `title` = 'characters_nav' LIMIT 1");
	$db->write_query("DELETE FROM `". $db->table_prefix ."templates` WHERE `title` = 'characters_mgmt' LIMIT 1");
	$db->write_query("DELETE FROM `". $db->table_prefix ."templates` WHERE `title` = 'characters_row' LIMIT 1");
	$db->write_query("DELETE FROM `". $db->table_prefix ."templates` WHERE `title` = 'characters_profile' LIMIT 1");
	$db->write_query("DELETE FROM `". $db->table_prefix ."templates` WHERE `title` = 'characters_profile_row' LIMIT 1");
	$db->write_query("DELETE FROM `". $db->table_prefix ."templates` WHERE `title` = 'characters_list' LIMIT 1");
	$db->write_query("DELETE FROM `". $db->table_prefix ."templates` WHERE `title` = 'characters_list_row' LIMIT 1");

}

function characters_activate() {

}

function characters_deactivate() {

}

// Characters nav template
$plugins->add_hook('usercp_menu', 'characters_nav');

function characters_nav() {

	global $mybb, $templates;

	eval('$mybb->user["characters_nav"]  = "'. $templates->get('characters_nav') .'";');
}

// Characters management
$plugins->add_hook('usercp_start', 'characters_mgmt');

function characters_mgmt() {

	global $mybb, $db, $templates, $theme, $lang, $header, $headerinclude, $footer, $usercpnav, $colspan, $username, $uid;

	// Show character management
	if($mybb->get_input('action') == 'characters') {

		// Get this user's characters
		$characters_current = $db->simple_select('characters', '*', "uid='". $mybb->user['uid'] ."'");
		$characters_rows = "";

		if($db->num_rows($characters_current) < 1) {
			$characters_rows = "No existing characters.<br>";
		} else {
			// Set up rows
			while($character = $db->fetch_array($characters_current)) {
				eval("\$characters_rows .= \"".$templates->get("characters_row")."\";");
			}
		}

		eval("\$characters_mgmt = \"".$templates->get("characters_mgmt")."\";");
		output_page($characters_mgmt);
	}

	// Create characters
	if($mybb->get_input('action') == 'characters_create') {

		$characters_create_fields = array(
			'name' => $_POST['character_name'],
			'uid' => $mybb->user['uid']
		);

		$db->insert_query('characters', $characters_create_fields);
		redirect("usercp.php?action=characters");
	}

	// Update characters
	if($mybb->get_input('action') == 'characters_update') {

		// Update names
		foreach($_POST['name'] as $cid => $name) {

			// Build out the fields
			$characters_update_fields['name'] = $name;

			// Update the row
			$db->update_query('characters', $characters_update_fields, '`cid` = '. $cid .' AND `uid` = '. $mybb->user['uid'], '1');
		}

		// Update links
		foreach($_POST['link'] as $cid => $link) {

			// Build out the fields
			$characters_update_fields['link'] = $link;

			// Update the row
			$db->update_query('characters', $characters_update_fields, '`cid` = '. $cid .' AND `uid` = '. $mybb->user['uid'], '1');
		}

		redirect("usercp.php?action=characters");
	}
}

// Make sure select is available in all the right places
$plugins->add_hook('newthread_start', 'characters_select');
$plugins->add_hook('editpost_start', 'characters_select');
$plugins->add_hook('newreply_start', 'characters_select');

// Set up characters select
function characters_select() {

	global $mybb, $db, $characters_select, $pid, $post, $style;

	if($mybb->get_input('pid')) {
		$pid = $mybb->get_input('pid');
		if(isset($style) && $style['pid'] == $pid && $style['type'] != 'f') {
			$post = &$style;
		} else {
			$post = get_post($pid);
		}
	} else {
		$post = NULL;
	}

	if(!$post || $post['uid'] == $mybb->user['uid']) {

		// Get this user's characters
		$characters_current = $db->simple_select('characters', '*', "uid='". $mybb->user['uid'] ."'");
		$characters_select = "<select name='cid' tabindex='2'><option value='0'>N/A</option>";

		if($db->num_rows($characters_current) >= 1) {
			// Set up rows
			while($character = $db->fetch_array($characters_current)) {
				
				// Select this option if this post currently has a character attached to it
				$selected = '';

				// Check against posts table
				if($post['cid'] == $character['cid']) {
					$selected = ' selected';
				}

				// Check against submitted data (for preview post)
				if($_POST['cid'] == $character['cid']) {
					$selected = ' selected';
				}

				// Output the option
				$characters_option = "<option value='". $character['cid'] ."'". $selected .">". $character['name'] ."</option>";
				$characters_select .= $characters_option;
			}

			$characters_select .= '</select>';
		}

	} else {
		$character_post = $db->simple_select('characters', '*', "cid='". $post['cid'] ."'");
		$character = $db->fetch_array($character_post);
		$characters_select = $character['name'];
	}
}

// Update post data
$plugins->add_hook('datahandler_post_update', 'characters_post_update');

function characters_post_update(&$handler) {

	if($_POST['cid']) {
		$handler->post_update_data['cid'] = $_POST['cid'];
	}
}

// Insert post data
$plugins->add_hook('datahandler_post_insert_post', 'characters_post_insert_post');
$plugins->add_hook('datahandler_post_insert_thread_post', 'characters_post_insert_post');

function characters_post_insert_post(&$handler) {

	if($_POST['cid']) {
		$handler->post_insert_data['cid'] = $_POST['cid'];
	}
}

// Postbit variables
$plugins->add_hook('postbit', 'characters_postbit');

function characters_postbit() {

	global $mybb, $db, $post;

	$character_query = $db->simple_select('characters', '*', '`cid` = '. $post['cid'], array('limit' => 1));
	$character = $db->fetch_array($character_query);

	if($character['link'] != '') {
		$mybb->user['character'] = '<a href="'. $character['link'] .'">'. $character['name'] .'</a>';
	} else {
		$mybb->user['character'] = $character['name'];
	}
}

// Member profile variables
$plugins->add_hook('member_profile_end', 'characters_profile');

function characters_profile() {

	global $mybb, $db, $uid, $characters, $templates, $theme;

	$character_rows;
	$characters_query = $db->simple_select('characters', '*', '`uid` = '. $uid);

	if($db->num_rows($characters_query) < 1) {
		$character_rows = '<li>No characters.</li>';
	} else {
		while($character = $db->fetch_array($characters_query)) {
			if($character['link'] != '') {
				$character['name'] = '<a href="'. $character['link'] .'">'. $character['name'] .'</a>';
			}
			eval("\$character_rows .= \"".$templates->get("characters_profile_row")."\";");
		}
	}

	eval("\$characters = \"".$templates->get("characters_profile")."\";");
}

// Character list variables
$plugins->add_hook('misc_start', 'characters_list');

function characters_list() {

	global $mybb, $db, $templates, $theme, $lang, $header, $headerinclude, $footer, $character_rows;

	if($mybb->get_input('action') == 'characters') {
		$characters_query = $db->simple_select('characters', '*', '', array('order_by'=>'name', 'order_dir'=>'ASC'));

		while($character = $db->fetch_array($characters_query)) {
			$user_query = $db->simple_select('users', 'uid, username', '`uid` = '. $character['uid']);
			$user = $db->fetch_array($user_query);

			$character['user'] = '<a href="'. $mybb->settings['bburl'] .'/member.php?action=profile&uid='. $character['uid'] .'">'. $user['username'] .'</a>';

			if($character['link'] != '') {
				$character['name'] = '<a href="'. $character['link'] .'">'. $character['name'] .'</a>';
			}

			eval("\$character_rows .= \"".$templates->get("characters_list_row")."\";");
		}

		eval("\$characters_list = \"".$templates->get("characters_list")."\";");
		output_page($characters_list);
	}
}